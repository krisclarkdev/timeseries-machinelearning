const express = require('express');
const swaggerUI = require('swagger-ui-express');
const YAML = require('yamljs');
const doc = require('./doc');
const swaggerDocument = require('./swagger.json');

const { loadDataModule, trainModelModule, predictModule, listModelsModule, trainOnMetricModule, predictNextModule } = require('./modules');

const app = express();
const port = 3000;

app.use(express.json());

// Define routes for each API operation
app.post('/data/train', loadDataModule);
app.post('/models/train', trainModelModule);
app.get('/models/list', listModelsModule);
app.post('/models/trainOnMetric', trainOnMetricModule);
app.post('/models/predict', predictModule);
app.post('/models/predictNext', predictNextModule);

// Serve the Swagger UI
app.use('/api-docs', swaggerUI.serve, swaggerUI.setup(swaggerDocument));

app.listen(port, () => {
  console.log(`Server listening on port ${port}`);
});

