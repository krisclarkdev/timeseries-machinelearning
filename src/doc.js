const swaggerYaml = `
swagger: '2.0'
info:
  version: '1.0.0'
  title: Time Series Prediction API
  description: API for training and predicting time series models
host: localhost:3000
basePath: /
tags:
  - name: data
    description: Operations related to data management
  - name: models
    description: Operations related to model training and management
  - name: prediction
    description: Operations related to time series prediction
schemes:
  - http
consumes:
  - application/json
produces:
  - application/json
paths:
  /data/train:
    post:
      tags:
        - data
      summary: Train a new model using time series data
      parameters:
        - in: body
          name: timeseriesData
          description: Array of time series data points
          schema:
            type: array
            items:
              type: number
          required: true
        - in: query
          name: iterations
          description: Number of training iterations to perform
          type: integer
          minimum: 1
      responses:
        '200':
          description: Model training started successfully
        '400':
          description: Invalid request format
        '500':
          description: Error occurred while training the model
  /models/list:
    get:
      tags:
        - models
      summary: List all saved models
      responses:
        '200':
          description: List of all saved models
          schema:
            type: array
            items:
              type: string
        '500':
          description: Error occurred while listing saved models
  /models/trainOnMetric:
    post:
      tags:
        - models
      summary: Train a new model using metric data from Wavefront
      parameters:
        - in: query
          name: wavefrontURL
          description: URL of the Wavefront instance
          type: string
          format: uri
          required: true
        - in: query
          name: wavefrontToken
          description: API token for the Wavefront instance
          type: string
          required: true
        - in: query
          name: metricName
          description: Name of the metric to use for training
          type: string
          required: true
        - in: query
          name: duration
          description: Duration of the training window in weeks
          type: integer
          minimum: 1
          required: true
        - in: query
          name: modelName
          description: Name to use for the new model
          type: string
          required: true
        - in: query
          name: iterations
          description: Number of training iterations to perform
          type: integer
          minimum: 1
          required: true
      responses:
        '200':
          description: Model training started successfully
        '400':
          description: Invalid request format
        '500':
          description: Error occurred while training the model
  /models/predict:
    post:
      tags:
        - prediction
      summary: Predict the next value for a time series using a trained model
      parameters:
        - in: body
          name: datapoints
          description: Array of time series data points
          schema:
            type: array
            items:
              type: number
          required: true
        - in: query
          name: model
          description: Name of the trained model to use for prediction
          type: string
          required: true
      responses:
        '200':
          description: Prediction successful
          schema:
            type: number
        '400':
          description: Invalid request format
        '500':
          description: Error occurred while making the prediction
  /models/predictNext:
    post:
      tags:
        - prediction
      summary: Predict the next value for a time series using a trained model and data from Wavefront
      parameters:
        - in: query
          name: wavefrontURL
          description: URL of the Wavefront instance
          type: string
          format: uri
          required: true
        - in: query
          name: wavefrontToken
          description: API token for the Wavefront instance
          type: string
          required: true
        - in: query
          name: metricName
          description: Name of the metric to use for prediction
          type: string
          required: true
        - in: query
          name: duration
          description: Duration of the prediction window in hours
          type: integer
          minimum: 1
          required: true
        - in: query
          name: modelName
          description: Name of the trained model to use for prediction
          type: string
          required: true
      responses:
        '200':
          description: Prediction successful
          schema:
            type: number
        '400':
          description: Invalid request format
        '500':
          description: Error occurred while making the prediction
`;
module.exports = swaggerYaml;

