const tf = require('@tensorflow/tfjs');
const fs = require('fs');
require('@tensorflow/tfjs-node');

async function loadData(data) {
  const dataArr = data.map(row => row.map(Number));
  return tf.tensor2d(dataArr);
}

function sliceIntoChunks(arr, chunkSize) {
    const res = [];
    for (let i = 0; i < arr.length; i += chunkSize) {
        const chunk = arr.slice(i, i + chunkSize);
        res.push(chunk);
    }
    return res;
}

function convertToTensor(data) {
  // convert data to a 2D array with shape [numSamples, numFeatures]
  const numSamples = data.length;
  const numFeatures = 1; // assuming one feature per sample
  const data2D = data.map(d => [d]);

  // convert the 2D array to a tensor with shape [numSamples, numFeatures]
  const tensor = tf.tensor2d(data2D, [numSamples, numFeatures]);

  return tensor;
}

async function trainModel(timeseriesData, modelName, iterations) {
  try {
    let epochs = 0;
    let loss = null;
    const model = tf.sequential();
    td = convertToTensor(timeseriesData)
    model.add(tf.layers.dense({ units: 64, inputShape: [1], activation: 'relu' }));
    model.add(tf.layers.dense({ units: 1, activation: 'linear' }));
    model.compile({ optimizer: tf.train.adam(), loss: 'meanSquaredError' });
    while (epochs < iterations) {
      const history = await model.fit(td, td, { epochs: 1 });
      loss = history.history.loss[0];
      console.log(`Epoch ${epochs + 1}/${iterations}, loss: ${loss}`);
      epochs++;
    }
    await model.save(`file://./models/${modelName}`);
    return { modelName, epochs, loss };
  } catch (err) {
    console.error(err);
    throw new Error(err.message);
  }
}

async function trainModelModule(req, res) {
  try {
    const timeseriesData = await loadData(req.query.timeseriesData);
    const modelName = req.query.modelName;
    const iterations = req.query.iterations;
    const result = await trainModel(timeseriesData, modelName, iterations);
    res.send(result);
  } catch (err) {
    console.error(err);
    res.status(500).send(err.message);
  }
}

module.exports = trainModel;
