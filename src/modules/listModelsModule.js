const fs = require('fs');

function listModels(req, res) {
  fs.readdir('models', (err, files) => {
    if (err) {
      console.error(err);
      res.status(500).send(err.message);
    } else {
      const modelIds = files.filter(file => fs.lstatSync(`models/${file}`).isDirectory());
      res.send(modelIds);
    }
  });
}

module.exports = listModels;

