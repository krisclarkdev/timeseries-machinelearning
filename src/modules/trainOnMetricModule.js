const axios = require('axios');
const trainModel = require('./trainModelModule');

async function downloadData(instance, token, metricName, duration) {
  const wavefrontUrl = `${instance}`;
  const query = `ts("${metricName}")`;
  const secondsInWeek = 604800;
  const startMillis = Date.now() - duration * 7 * secondsInWeek * 1000;
  const endMillis = Date.now();
  const url = `${wavefrontUrl}/api/v2/chart/api?s=${startMillis}&e=${endMillis}&q=${query}`;
  const headers = { Authorization: `Bearer ${token}` };
  const response = await axios.get(url, { headers });
  const data = response.data.timeseries[0].data;
  return data.map(([timestamp, value]) => value);
}

async function trainOnMetric(req, res) {
  try {
    const instance = req.query.wavefrontURL;
    const token = req.query.wavefrontToken;
    const metricName = req.query.metricName;
    const duration = req.query.duration;
    const modelName = req.query.modelName;
    const iterations = req.query.iterations;
    const data = await downloadData(instance, token, metricName, duration);
    //console.log(trainModule)
    //const timeseriesData = await trainModule.loadData(data);
//    console.log(trainModelModule.trainModel(data, modelName, iterations))
    await trainModel(data, modelName, iterations);
    //await trainModule.trainModelModule(data, modelName, iterations);
    res.send({ status: 'Training started' });
  } catch (err) {
    console.error(err.response);
    res.status(500).send(err.message);
  }
}

module.exports = trainOnMetric;

