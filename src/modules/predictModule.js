const tf = require('@tensorflow/tfjs');
const fs = require('fs');

async function predict(datapoint, modelName) {
  const model = await tf.loadLayersModel(`file://models/${modelName}/model.json`);
  const input = tf.tensor2d(datapoint, [1, datapoint.length]);
  const output = model.predict(input).dataSync()[0];
  return output;
}

async function predictModule(req, res) {
  try {
    const datapoint = req.query.datapoint;
    const modelName = req.query.model;
    const predictedOutput = await predict(datapoint, modelName);
    res.send({ predictedOutput });
  } catch (err) {
    console.error(err);
    res.status(500).send(err.message);
  }
}

module.exports = predictModule;

