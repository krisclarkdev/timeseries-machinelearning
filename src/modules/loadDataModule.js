function loadData(data) {
  // check if data is an array
  if (!Array.isArray(data)) {
    throw new Error('Data must be an array');
  }

  // check if each element in data is a valid number
  const processedData = data.map((d) => {
    if (isNaN(d)) {
      throw new Error('Data must contain only valid numbers');
    }
    return Number(d);
  });

  return processedData;
}

module.exports = loadData;

