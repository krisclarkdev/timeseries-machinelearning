const axios = require('axios');
//const predictModule = require('./predictModule');
const tf = require('@tensorflow/tfjs');
const fs = require('fs');
require('@tensorflow/tfjs-node');

async function sendMetric(wavefrontURL, wavefrontToken, metricName, metricValue, tags) {
  // Construct the URL for the Wavefront API endpoint
  const apiUrl = `${wavefrontURL}/report`;

  // Construct the headers for the Wavefront API request
  const headers = {
    Authorization: `Bearer ${wavefrontToken}`
  };

  // Construct the payload for the Wavefront API request
  const payload = `${metricName} ${metricValue} ${Math.floor(Date.now() / 1000)} source=wavefront-nodejs `;
  for (const [key, value] of Object.entries(tags)) {
    payload += `${key}=${value} `;
  }
  payload.trim();

  // Send the metric to Wavefront using the Axios HTTP client library
  try {
    const response = await axios.post(apiUrl, payload, { headers });
    console.log(`Metric sent to Wavefront: ${metricName}=${metricValue}`);
    console.log(response.data)
    return response.data;
  } catch (error) {
    console.error(`Failed to send metric to Wavefront: ${metricName}=${metricValue}`);
    throw error;
  }
}

function convertToTensor(data) {
  // convert data to a 2D array with shape [numSamples, numFeatures]
  const numSamples = data.length;
  const numFeatures = 1; // assuming one feature per sample
  const data2D = data.map(d => [d]);

  // convert the 2D array to a tensor with shape [numSamples, numFeatures]
  const tensor = tf.tensor2d(data2D, [numSamples, numFeatures]);

  return tensor;
}

async function predict(datapoint, modelName) {
  console.log(datapoint)
  console.log(modelName)
  const model = await tf.loadLayersModel(`file://./models/${modelName}/model.json`);
  const input = tf.tensor2d(datapoint, [1, datapoint.length]);
  const output = model.predict(input).dataSync()[0];
  console.log(output)
  return output;
}

async function downloadData(instance, token, metricName, duration) {
  const wavefrontUrl = `${instance}`;
  const query = `ts("${metricName}")`;
  const startMillis = Date.now() - duration * 60 * 60 * 1000;
  const endMillis = Date.now();
  const url = `${wavefrontUrl}/api/v2/chart/api?s=${startMillis}&e=${endMillis}&q=${query}`;
  const headers = { Authorization: `Bearer ${token}` };
  const response = await axios.get(url, { headers });
  const data = response.data.timeseries[0].data;
  return data.map(([timestamp, value]) => value);
}

async function predictNext(req, res) {

  try {
    const instance = req.query.wavefrontURL;
    const token = req.query.wavefrontToken;
    const metricName = req.query.metricName;
    const modelName = req.query.modelName;
    const duration = req.query.duration;
    const data = await downloadData(instance, token, metricName, duration);
    const lastDatapoint = data[data.length - 1];
    let test = new Array()
    test.push(lastDatapoint)
    const t = convertToTensor(data)
    const predictedValue = await predict(test, modelName);

    res.send(`${req.query.metricName}.predicted ${predictedValue} source=tensorflow`)
  } catch (err) {
    console.error(err);
    res.status(500).send(err.message);
  }
}

module.exports = predictNext;
