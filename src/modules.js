const loadDataModule = require('./modules/loadDataModule');
const trainModelModule = require('./modules/trainModelModule');
const predictModule = require('./modules/predictModule');
const listModelsModule = require('./modules/listModelsModule');
const trainOnMetricModule = require('./modules/trainOnMetricModule');
const predictNextModule = require('./modules/wavefrontPredictModule');

module.exports = {
  loadDataModule,
  trainModelModule,
  predictModule,
  listModelsModule,
  trainOnMetricModule,
  predictNextModule,
};

