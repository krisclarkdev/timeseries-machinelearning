# Time Series Machine Learning

This repository is a web based utility that uses tensorflow to train against Aria Operations for Applications timeseries data.  Once a your model is trained you can then use telegraf to point at /models/predictNext to return what your model thinks the next value should be.  It returns in wavefront data format making it easy to integrate with telegraf and send the resulting metric to Aria Operations for Applications

## Table of Contents

# Table of Contents
1. [Standalone Installtion](#standalone-installation)
2. [Kubernetes Deployment](#kubernetes-installation)
3. [Usage](#usage)
4. [Contributing](#contributing)
6. [License](#license)

# Standalone installation

```
git clone https://gitlab.com/krisclarkdev/timeseries-machinelearning.git
cd timeseries-machinelearning
npm install
node ./index.js
```

# Kubernetes installation

```
git clone https://gitlab.com/krisclarkdev/timeseries-machinelearning.git
cd timeseries-machinelearning
kubectl create -f ./tsml.yaml
```

Note: I havevn't tested this on k8s yet

# Usage

API documentation can be found at http://localhost:3000/api-docs

The basic workflow is this

1. Log into Aria Operations for Applications and find a metric name that you'd like to train against
2. Train against that dataset.  Input values for wavefront URL, wavefront token, metric name, duration (weeks), name of the new model, how many iterations to train for

```
curl -X 'POST' \
  'http://localhost:3000/models/trainOnMetric?wavefrontURL=https%3A%2F%2FCHANGEME.wavefront.com&wavefrontToken=CHANGEME&metricName=CHANGEME&duration=1&modelName=CHANGEME&iterations=CHANGEME' \
  -H 'accept: application/json' \
  -d ''
```

3. Verify the model has been saved

```
curl -X 'GET' \
  'http://localhost:3000/models/list' \
  -H 'accept: application/json'
```

4. Predict the next value.  Input values for the metric you'd like to check against, wavefront URL, wavefront token, model name

```
curl -X 'POST' \
  'http://localhost:3000/models/predictNext?wavefrontURL=https%3A%2F%2FCHANGEME.wavefront.com&wavefrontToken=CHANGEME&metricName=CHANGEME&duration=CHANGEME&modelName=CHANGEME' \
  -H 'accept: application/json' \
  -d ''
```

5. If happy with the results setup telegraf to scrape the /predictNext endpoint at an interval of your choosing

# Contributing

Contributions to this repository are welcome. If you find a bug or have a suggestion for a new feature, please open an issue on GitLab. If you would like to contribute code, please fork the repository and create a pull request with your changes.

# License

This repository is licensed under the MIT License. See the LICENSE file for details.
